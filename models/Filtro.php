<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Filtro extends Model
{
    public $textoBuscar;
    public $particular;
    public $empresa;
   
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           
            [['textoBuscar'], 'string'],
             [['particular','empresa'], 'boolean'],
          
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'textoBuscar' => '',
//            'tipoBuscar' => '',
        ];
    }


}
