<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;


/**
 * This is the model class for table "talentos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $profesion
 * @property string|null $tipo
 * @property string|null $lanzadera
 * @property int|null $telefono
 * @property string|null $email
 * @property string|null $web
 * @property string|null $linkedin
 * @property string|null $foto
 * @property string|null $sobremi
 * @property string|null $carta
 */
class Talentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFile;
 
     
    public static function tableName()
    {
        return 'talentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'telefono'], 'integer'],
            [['sobremi', 'carta'], 'string'],
            [['particular', 'empresa'], 'boolean'],
            [['nombre', 'profesion', 'tipo', 'lanzadera', 'foto'], 'string', 'max' => 300],
            [['apellidos', 'email', 'web', 'linkedin'], 'string', 'max' => 200],
            [['id'], 'unique'],
        ];
       return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'profesion' => 'Profesion',
            'tipo' => 'Tipo',
            'particular' => 'Particular',
            'empresa' => 'Empresa',
            'lanzadera' => 'Lanzadera',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'web' => 'Web',
            'linkedin' => 'Linkedin',
            'foto' => 'Foto',
            'sobremi' => 'Sobremi',
            'carta' => 'Carta',
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
//            $this->imageFile->saveAs('../web/img/Medio Cudeyo/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
              $this->imageFile->saveAs('../web/img/'. $this->lanzadera.'/'.$this->apellidos.$this->nombre.$this->id.'.png');
            return true;
        } else {
            return false;
        }
    }
}


    
 

