<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;

$url = Yii::getAlias("@web") . '/img/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
	<title>Listado Perfiles</title>
	<style type="text/css">
                .ayto{
			padding-top: 10px;
		}
		.logo{
			padding-top: 10px;
		}
		.cabecera{
			margin-bottom: 60px;
			padding: 40px 0;
		}
		.pie{
			height: 220px;
			margin-top: 60px;
		}
		.gobierno{
			text-align: right;
			padding-top: 60px;
		}
		.emcan{
			text-align: center;
			padding-top: 90px;
		}
		.cantabria{
			padding-top: 30px;
		}
		.textopie{
			padding: 80px 20px;
		}
		p.enelpie{
			font-weight: bold;
			font-size: 1.2em;
		}
                th{
                    padding:8px;    
                    width: 240px;
                    background-color: #f2f2f2;
                }
                td{
                    padding:8px;
                }
                
        </style>
</head>
<body>
    
    <div class="col-md-12" style="padding-bottom:140px; padding-top:80px">
        <div class="camino col-md-4">
                <img src="<?=Url::to('@web/img/lg_camino.png')?>" alt="camino" width="390px" />
        </div>

        <div class="ayto col-md-4">
                <p style="font-weight:bold; font-size:1.3em; text-align:center; padding: 44px 28px 0;">Lanzadera de Empleo y Emprendimiento Solidario del Ayuntamiento de Medio Cudeyo</p>
        </div>

        <div class="ayto col-md-2">
                <img src="<?=Url::to('@web/img/lg_ayuntamiento.png')?>" alt="lanzaderas" width="156px" />
        </div>

        <div class="logo col-md-2 col-12">
                <a href="https://www.cantabriaemprendedora.es/es/lanzaderas-cantabria-que-son-y-como-participar" target="_blank"><img src="<?=Url::to('@web/img/lg_lanzaderas3.png')?>" alt="lanzaderas" width="120px" /></a>
        </div>
    </div>
    
    
    
    <div style="padding:0 120px">
		
        <div class="col-md-12" style="text-align:center; font-size:1.6em; font-weight:bold; padding-bottom:30px;">
                <p style="">Autorización</p>
        </div>

           

        <div class="col-md-12" style="text-align:justify; font-size:1.2em;padding-bottom: 30px">
            <p>AUTORIZO al Ayuntamiento de Medio Cudeyo la utilización de los datos que he publicado voluntariamente en la web 
                htpp//www.talentoslanzadera.mediocudeyo.es, así como la imagen personal grabada o retratada de este modo en la web y 
                redes sociales que este organismo gestiona directamente para la difusión del programa de Lanzaderas de Empleo y Emprendimiento 
                Solidario, sin que dicha autorización esté sometida a ningún plazo temporal ni esté restringida al ámbito nacional de ningún país.</p>
            <p>La cesión de información y de la imagen personal/profesional del manifestante tiene carácter gratuito y contribuirá a una mayor
                difusión de su perfil profesional para que las empresas encuentren profesionales, o bien los particulares encuentren los servicios
                que buscan de las empresas. Haciendose visibles también contribuyen a una mejor imagen del proyecto en que el autorizante
                participe.</p>
            <p>Las personas que han publicado sus perfiles profesionales en esta web, podrán darse de baja cuando lo decidan y lo
                comunicarán a la Agencia de Desarrollo Local del Ayuntamiento de Medio Cudeyo en el momento que se efectue la baja.</p>
        </div>
        
        
        
        <table class="col-12" border="1px solid black">
            <tr>
                <th colspan="2">INFORMACIÓN BÁSICA SOBRE PROTECCIÓN DE DATOS PERSONALES<br>
                    En cumplimiento del Reglamento General de Protección de Datos (Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de
                    abril de 2016), se informa:
                </th>
            </tr>
            <tr>
                <th>Responsable del tratamiento</th>
                <td>Ayuntamiento de Medio Cudeyo, a través de la Agencia de Desarrollo Local.</td>
            </tr>
            <tr>
                <th>Finalidad</th>
                <td>Con los datos recogidos en el presente formulario se realizará una actividad de tratamiento para la gestión de las
                    políticas activas de empleo y de la intermediación.</td>
            </tr>
            <tr>
                <th>Legitimación</th>
                <td>Cumplimiento de una misión en interés público.</td>
            </tr>
            <tr>
                <th>Destinatarios</th>
                <td>Los datos que se incluyan en los registros de la web mencionada no se transmitirán a terceras entidades o personas
                ajenas al proyecto indicado. Se ha creado con la finalidad de facilitar la búsqueda de empleo para los/as
                participantes del proyecto Lanzadera de Empleo y Emprendimiento Solidario del Ayuntamiento de Medio Cudeyo.
                Las personas destinatarias se han inscrito voluntariamente en este espacio, igualmente si así lo deciden, se darán
                de baja.</td>
            </tr>
            <tr>
                <th>Derechos</th>
                <td>Me reservo los derechos de acceso, modificación y cancelación conforme a la Ley Orgánica 1/1982, de 5 de mayo, 
                    de Protección Civil del Derecho al Honor, a la Intimidad Personal y Familiar y a la Propia Imagen.</td>
            </tr>
        </table>
   
    
        <div style="padding-top:30px;">
        <p>Y para que conste y surta efectos, acepto la presente autorización al amparo de lo dispuesto en la ley arriba mencionada.</p>
    </div>
    
</div>
 
    <div class="pie col-md-12">
        <div class="textopie col-md-5">
                <p class="enelpie">Agencia de Desarrollo local del Ayuntamiento de Medio Cudeyo</p>
                <p class="enelpie">Tlf: 942 522 833</p>
        </div>
        <div class="gobierno col-md-3">
            <img src="<?=Url::to('@web/img/lg_gobierno2.png')?>" alt="gobierno" width="160px"/>
        </div>
        <div class="emcan col-md-2">
            <a href="https://www.empleacantabria.es/" target="_blank"><img src="<?=Url::to('@web/img/lg_emcan2.png')?>" alt="emcan" width="200px"/></a>
        </div>
        <div class="cantabria col-md-2">
            <img src="<?=Url::to('@web/img/lg_cantabria2.png')?>" alt="cantabria" width="150px"/>
        </div>
    </div>
    
 
</body>
</html>