<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<title>Listado Perfiles</title>
	<style type="text/css">
                main{
                        
                }
		.ayto{
			padding-top: 10px;
		}
		.logo{
			padding-top: 10px;
		}
		.camino{
			
		}
		.cabecera{
			margin-bottom: 60px;
			padding: 40px 0;
		}
		.pie{
			height: 220px;
			margin-top: 120px;
		}
		.gobierno{
			text-align: right;
			padding-top: 60px;
		}
		.emcan{
			text-align: center;
			padding-top: 90px;
		}
		.cantabria{
			padding-top: 30px;
		}
		.textopie{
			padding: 80px 20px;
		}
		p.enelpie{
			font-weight: bold;
			font-size: 1.2em;
		}
		
	</style>
</head>
<body>
    
    
    <div class="col-md-12" style="padding-bottom:140px; padding-top:80px">
        <div class="camino col-md-4">
                <img src="<?=Url::to('@web/img/lg_camino.png')?>" alt="camino" width="390px" />
        </div>

        <div class="ayto col-md-4">
                <p style="font-weight:bold; font-size:1.3em; text-align:center; padding: 44px 28px 0;">Lanzadera de Empleo y Emprendimiento Solidario del Ayuntamiento de Medio Cudeyo</p>
        </div>

        <div class="ayto col-md-2">
                <img src="<?=Url::to('@web/img/lg_ayuntamiento.png')?>" alt="lanzaderas" width="156px" />
        </div>

        <div class="logo col-md-2 col-12">
                <a href="https://www.cantabriaemprendedora.es/es/lanzaderas-cantabria-que-son-y-como-participar" target="_blank"><img src="<?=Url::to('@web/img/lg_lanzaderas3.png')?>" alt="lanzaderas" width="120px" /></a>
        </div>
    </div>
    
    <div class="row">
        <div class="col col-sm-12" style="text-align: right;font-weight: bold;font-size:20px;margin-bottom:40px;padding-right:100px">
            <span class="fas fa-undo-alt">
                <?= Html::a('Volver', Yii::$app->request->referrer)?>
            </span>
        </div>
    </div>
     
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => "",
            'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('_post_todo',['model' => $model]);
                  },
            ]);         
        ?>
    
        <div class="pie col-md-12">
            <div class="textopie col-md-5">
                    <p class="enelpie">Agencia de Desarrollo local del Ayuntamiento de Medio Cudeyo</p>
                    <p class="enelpie">Tlf: 942 522 833</p>
            </div>
            <div class="gobierno col-md-3">
                <img src="<?=Url::to('@web/img/lg_gobierno2.png')?>" alt="gobierno" width="160px"/>
            </div>
            <div class="emcan col-md-2">
                <a href="https://www.empleacantabria.es/" target="_blank"><img src="<?=Url::to('@web/img/lg_emcan2.png')?>" alt="emcan" width="200px"/></a>
            </div>
            <div class="cantabria col-md-2">
                <img src="<?=Url::to('@web/img/lg_cantabria2.png')?>" alt="cantabria" width="150px"/>
            </div>
        </div>
    
 
</body>
</html>