
<?php
use yii\helpers\Url;
use yii\helpers\Html;

$Apelnomb = $model->nombre." ".$model->apellidos;
$rutaFoto = '@web/img/'.$model->lanzadera.'/'.$model->apellidos.$model->nombre.$model->id.'.png';

?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<div class="col-md-12" style="margin-top: 60px;padding:0 30px">
    <div class="perfil" style="margin: 40px 240px; padding:20px">
        <div class="row no-gutters">
            <div class="col-md-4" style="text-align:center">
                <img src="<?=Url::to($rutaFoto)?>" class="card-img" alt="" width="160px;" height="180px" style="border-radius: 5%;">
             </div>
            <div class="col-md-8" style="padding:10px 30px 0px; text-align:right">
                <div class="card-body">
                    <p style="font-size:1.7em; font-weight:bold"><?= $Apelnomb ?></p>
                    <p style="color:#D62020; font-size:1.7em; font-weight:bold; font-family:serif;"><?= $model->profesion ?></p>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-12" style="padding: 16px 30px; text-align:justify;">
                <p><?= $model->sobremi ?></p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-12" style="padding: 0 30px; text-align:right;">
                <!--<button type="button" class="botonver btn" style="width: 160px; background-color: #247386; "><a href="file:///C:/Users/veroc/OneDrive/Escritorio/COSINAS/lanzadera_empleo/perfil_mostrar.html" style="text-decoration:none; color:white; font-weight:bold;">Ver</a></button>-->
                  <?=Html::a(Yii::t('app','Ver'), ['/site/mostrartalento', 'id' => $model->id],['class' => 'botonver btn','style' => 'width:160px;text-decoration: none;color: white; font-weight: bold;background-color: #247386']) ?>
            </div>
        </div>
    </div>
</div>


