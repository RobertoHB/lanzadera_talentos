<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\Link;



$Apelnomb = $model->nombre." ".$model->apellidos;
$rutaFoto = '@web/img/'.$model->lanzadera.'/'.$model->apellidos.$model->nombre.$model->id.'.png';
$linkedin = $model->linkedin;
//echo $linkedin;
//exit;

?>
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <div class="col-md-12" style="padding:0 120px;">
            <div class="col-md-4">
		<img src="<?=Url::to($rutaFoto)?>" alt="candidato" height="220px;" width="180px;" style="border-radius: 3%; margin-left: 40px" />
            </div>
            <div class="col-md-8" style=" margin-top: 40px; text-align: right">
                <p style="font-size:1.7em; font-weight:bold"><?= $Apelnomb ?></p>
                <p style="color:#D62020; font-size:1.7em; font-weight:bold; font-family:serif;"><?= $model->profesion ?></p>
                <p style="font-size:1.7em; font-weight:bold">Lanzadera de Medio Cudeyo</p>
            </div>
	</div>

	<div class="col-md-12" style="padding: 50px 140px;text-align: justify;">
            <p style="font-size: 1.3em; font-weight: bold;">ALGO SOBRE MI</p>
            <p style="font-size: 1.2em; font-weight: bold"><?= $model->sobremi?></p>
            <p style="font-size: 1.3em; font-weight: bold; padding-top: 40px;">MI CARTA DE PRESENTACION</p>
            <p style="font-size: 1.2em;"><?= $model->carta ?></p>
	</div>
	
	<div class="col-md-12" style="padding: 40px 140px;">
            <p style="font-weight: bold; padding-bottom: 20px;">ME PUEDES CONTACTAR AQUÍ:</p>
            <div class="row">
                <div class="col-md-6" style="text-align: center">
                    <!--<p style="font-size: 1.3em; font-weight: bold; text-align: left;"><?= $model->email?></p>-->
                    <i class="fas fa-envelope fa-4x" style="color: #337AB7"></i><p style="font-size: 20px;"><?= $model->email?></p>
                    <i class="fas fa-phone-square fa-4x" style="color: #337AB7"></i><p style="font-size: 20px"><?= $model->telefono?></p>
                </div>
                <div class="col-md-6" style="text-align: center">
                    <a href="<?= $linkedin ?>" target="_blank"><i class="fab fa-linkedin fa-4x"></i></a><span><p style="font-size: 20px;">Linkedin</p>
                    <a href="<?= $model->web ?>" target="_blank"><i class="fab fa-chrome fa-4x"></i></a><p style="font-size: 20px;">Visita mi Web</p>
                </div>        
            </div>
        </div>        

