<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


$itemSiNo = ['Si'=>'Si','No'=> 'No'];
$itemFecha = ['2016-2017'=>'2016-2017','2017-2018'=>'2017-2018','2018-2019'=>'2018-2019','2019-2020'=>'2019-2020']

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>


<div style="padding: 100px 200px">
  
    <h1>Regístrate</h1>
    <?php $form = ActiveForm::begin([
        'method' => 'post',
     'id' => 'formulario_autorizo',
     'enableClientValidation' => true,
     'enableAjaxValidation' => false,
    ]);
    ?>
    <div>
        <h4 style="color:blue"><?= $msg ?></h4> 
    </div>
    <div id="mensaje" style="display:none">
        <h4 style="color:red">Sitiéndolo mucho este espacio se ha diseñado para participantes del programa de la Lanzadera de Empleo</h4> 
        
    </div>
    
    <div class="form-group">
        <label>¿Has pertenecido a alguna Lanzadera de empleo?</label>
        <select id="pertenece">
            <option value="0">Si</option>
            <option value="1">No</option>
        </select>
       
    </div>
   
    <div class="form-group campo" style="width: 200px;">
      <?= $form->field($model, 'fechalanzadera')->dropDownList($itemFecha, ['prompt' => '' ])->label('¿Cuándo?'); ?>
    </div>

    <div class="form-group campo" style="width: 400px">
     <?= $form->field($model, "nombre")->input("text") ?>   
    </div>

    <div class="form-group campo" style="width: 400px">
     <?= $form->field($model, "apellidos")->input("text") ?>   
    </div>

    <div class="form-group campo" style="width: 400px">
     <?= $form->field($model, "movil")->input("text") ?>   
    </div>
     <div class="form-group campo" style="width: 400px">
     <?= $form->field($model, "email")->input("email") ?>   
    </div>
 
    <div class="campo">   
        <input type="checkbox" id="marcaconsetimiento" /><a style="padding-left: 10px;" href="<?= Url::to('autorizacion')?>" target="_blank">Acepto la política de Privacidad y Autorizo la exposición de mis datos</a> 
    </div>
     
</div>
    
    <?= Html::submitButton("Aceptar y Enviar", ["class" => "btn btn-primary btn-lg", "style" =>'margin-left:200px','id' =>'btn_enviar']) ?>

    <?php  ActiveForm::end(); ?>

</div>
<script>
$( document ).ready(function() {
     $('button').attr('disabled','disabled');
    $('#pertenece').change(function(event) { 

    if ($('#pertenece').val() == 0){
         $('#mensaje').css('display','none');
    }else{
         $('#mensaje').show();
         $('input').attr('disabled','disabled');
         $('checkbox').attr('disabled','disabled');
         $('button').attr('disabled','disabled');
         $('select').attr('disabled','disabled');
    }

    });
   
    $('#marcaconsetimiento').change(function(event) {  
         $('#marcaconsetimiento').each(function(){
            if($(this).is(':checked')){ 
                $('button').removeAttr('disabled');
            }else{
                $('button').attr('disabled','disabled');
            }
        });
    });
//    $('#formulario_autorizo').on('submit', function() {
//         $('#mensaje>h4').html('hola');
//         $('#mensaje').show();
//    });
  
     
}); 
    
</script>    