<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;

$url = Yii::getAlias("@web") . '/img/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
	<title>Listado Perfiles</title>
	<style type="text/css">
                .ayto{
			padding-top: 10px;
		}
		.logo{
			padding-top: 10px;
		}
		.cabecera{
			margin-bottom: 60px;
			padding: 40px 0;
		}
		.pie{
			height: 220px;
			margin-top: 60px;
		}
		.gobierno{
			text-align: right;
			padding-top: 60px;
		}
		.emcan{
			text-align: center;
			padding-top: 90px;
		}
		.cantabria{
			padding-top: 30px;
		}
		.textopie{
			padding: 80px 20px;
		}
		p.enelpie{
			font-weight: bold;
			font-size: 1.2em;
		}
        </style>
</head>
<body>
    
    <div class="col-md-12" style="padding-bottom:60px; padding-top:80px">
        <div class="camino col-md-4">
                <img src="<?=Url::to('@web/img/lg_camino.png')?>" alt="camino" width="390px" />
        </div>

        <div class="ayto col-md-4">
                <p style="font-weight:bold; font-size:1.3em; text-align:center; padding: 44px 28px 0;">Lanzadera de Empleo y Emprendimiento Solidario del Ayuntamiento de Medio Cudeyo</p>
        </div>

        <div class="ayto col-md-2">
                <img src="<?=Url::to('@web/img/lg_ayuntamiento.png')?>" alt="lanzaderas" width="156px" />
        </div>

        <div class="logo col-md-2 col-12">
                <a href="https://www.cantabriaemprendedora.es/es/lanzaderas-cantabria-que-son-y-como-participar" target="_blank"><img src="<?=Url::to('@web/img/lg_lanzaderas3.png')?>" alt="lanzaderas" width="120px" /></a>
        </div>
    </div>
    
     <div class="row">
        <div class="col col-sm-12" style="text-align: right;font-weight: bold;font-size:20px;margin-bottom:40px;padding-right:100px">
            <span class="fas fa-undo-alt">
                <?= Html::a('Volver', Yii::$app->request->referrer)?>
            </span>
        </div>
    </div>
    
    <div style="padding:0 120px">
		
        <div class="col-md-12" style="text-align:center; font-size:1.6em; font-weight:bold; padding-bottom:30px;">
                <p style="">Qué es una Lanzadera de Empleo</p>
        </div>

        <div class="col-md-12" style="text-align:justify; font-size:1.2em">
                <p>Una lanzadera es un equipo de 20 personas desempleadas con diferentes edades, perfiles formativos y trayectorias laborales, que durante seis meses se reúne de forma voluntaria y gratuita al programa para entrenar una búsqueda de trabajo innovadora, proactiva, organizada, visible y solidaria. No están solos en este reto, cuentan con la ayuda de un Técnico/a, que les acompaña y guía durante seis meses.</p>
        </div>

        <div class="col-md-12" style="text-align:center; padding:80px 0">
                <img src="<?=Url::to('@web/img/queesunalanzadera.png')?>" width="600px"/>
        </div>

        <div class="col-md-12" style="text-align:justify; font-size:1.2em">
                <p>Con la diversidad de perfiles se logra que haya una cultura colaborativa, que los participantes compartan conocimientos y se ayuden mutuamente para reforzar sus habilidades, cambiar de actitud y conseguir un fin común: lograr trabajo, ya sea por cuenta ajena o propia.</p>
                <p>Las lanzaderas de empleo y emprendimiento solidario están concebidas para permitir incrementar las posibilidades de sus participantes para encontrar empleo, mejorando las estrategias de auto postulación de cara a la contratación por terceros, o bien uniéndose a otras personas en situación de desempleo para encontrar un empleo individual o colectivo, o bien creando pequeñas empresas para el desarrollo del autoempleo.</p>
                <p>El programa "Lanzaderas de Empleo y Emprendimiento Solidario" nace en 2012 gracias a la iniciativa de José María Pérez "Peridis" al frente de la Fundación Santa María la Real. El Servicio Cántabro de Empleo fue la primera entidad, pública o privada, en acoger el programa, incluyéndolo entre sus políticas activas de empleo.</p>
                <button type="button" class="btn" style="background-color:#58A8BB; width:30%; margin:30px 20px; height:40px;"><a href="http://mediocudeyo.es/web/empleo/" target="_blank" style="text-decoration:none; color:white; font-size:1.5em">Web Empleo Medio Cudeyo</a></button>
        </div>
    </div>
    
    <div class="pie col-md-12">
        <div class="textopie col-md-5">
                <p class="enelpie">Agencia de Desarrollo local del Ayuntamiento de Medio Cudeyo</p>
                <p class="enelpie">Tlf: 942 522 833</p>
        </div>
        <div class="gobierno col-md-3">
            <img src="<?=Url::to('@web/img/lg_gobierno2.png')?>" alt="gobierno" width="160px"/>
        </div>
        <div class="emcan col-md-2">
            <a href="https://www.empleacantabria.es/" target="_blank"><img src="<?=Url::to('@web/img/lg_emcan2.png')?>" alt="emcan" width="200px"/></a>
        </div>
        <div class="cantabria col-md-2">
            <img src="<?=Url::to('@web/img/lg_cantabria2.png')?>" alt="cantabria" width="150px"/>
        </div>
    </div>
    
 
</body>
</html>