<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<title>Listado Perfiles</title>
	<style type="text/css">
		.titulo{
			text-align: right;
			padding-top: 50px;
		}
		h3{
			font-weight: bold;
		}
		.logo{
			padding-top: 10px;
		}
		.camino{
			text-align: right;
		}
		.cabecera{
			margin-bottom: 60px;
		}
		.pie{
			height: 220px;
			margin-top: 120px;
		}
		.gobierno{
			text-align: right;
			padding-top: 60px;
		}
		.emcan{
			text-align: center;
			padding-top: 90px;
		}
		.cantabria{
			padding-top: 30px;
		}
		.textopie{
			padding: 90px 120px;
			text-align: center;
		}
		p.enelpie{
			font-weight: bold;
		}
		
	</style>
</head>
<body>

	<div class="cabecera col-md-12">
		<div class="camino col-md-4">
                    <img src="<?=Url::to('@web/img/lg_camino.png')?>" alt="camino" width="300px" />
		</div>
		<div class="titulo col-md-6">
			<h4>LANZADERAS DE CANTABRIA</h4>
		</div>

		<div class="logo col-md-2 col-12">
                    <!--<img src="<?=Url::to('@web/img/lg_lanzaderas2.png')?>" alt="lanzaderas" width="120px" />-->
                     <a href="https://www.cantabriaemprendedora.es/es/lanzaderas-cantabria-que-son-y-como-participar" target="_blank"><img src="<?=Url::to('@web/img/lg_lanzaderas2.png')?>" alt="lanzaderas" width="120px" /></a>
		</div>
	</div>
    <div class="row">
        <div class="col col-sm-12" style="text-align: right;font-weight: bold;font-size:20px;">
            <span class="fas fa-undo-alt">
                <?= Html::a('Volver', Yii::$app->request->referrer)?>
            </span>
        </div>
    </div>
	
  

     
           <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'summary' => "",
                'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('_post_todo',['model' => $model]);
                      },
                ]);     
    
    
    ?>

	<div class="pie col-md-12">
		<div class="textopie col-md-6">
			<p class="enelpie">Lanzadera de Empleo y Emprendimiento Solidario del Ayuntamiento de Medio Cudeyo</p>
		</div>
		<div class="gobierno col-md-2">
                    <img src="<?=Url::to('@web/img/lg_gobierno2.png')?>" alt="gobierno" width="160px"/>
		</div>
		<div class="emcan col-md-2">
                       <a href="https://www.empleacantabria.es/" target="_blank"><img src="<?=Url::to('@web/img/lg_emcan2.png')?>" alt="emcan" width="200px"/></a>
			<!--<img src="<?=Url::to('@web/img/lg_emcan2.png')?>" alt="emcan" width="200px"/>-->
		</div>
		<div class="cantabria col-md-2">
			<img src="<?=Url::to('@web/img/lg_cantabria2.png')?>" alt="cantabria" width="150px"/>
		</div>
	</div>

</body>
</html>