<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$tipo = ['Empresa'=>'Empresa','Particular'=>'Particular'];
$lanzadera = ['Medio Cudeyo'=>'Medio Cudeyo'];
$ruta_foto_perfil = Url::to('@web/img/'.$model->lanzadera.'/'.$model->apellidos.$model->nombre.$model->id.'.png'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">	
	<title>Registro de Candidatos</title>
	<style type="text/css">
		.titulo{
			text-align: right;
			padding-top: 64px;
		}
		h3{
			font-weight: bold;
		}
		.logo{
			padding-top: 10px;
		}
		.camino{
			text-align: right;
		}
		.cabecera{
			margin-bottom: 60px;
		}
		form{
			/*padding: 40px 80px;*/
		}
		.pie{
			height: 220px;
			/*border-top: 1px solid black;*/
			margin-top: 120px;
		}
		.gobierno{
			text-align: right;
			padding-top: 60px;
		}
		.emcan{
			text-align: center;
			padding-top: 90px;
		}
		.cantabria{
			padding-top: 30px;
		}
		.textopie{
			padding: 80px 120px;
			text-align: center;
		}
		p{
			font-weight: bold;
		}
		label{
			padding-top: 24px;
		}
		.enviar{
			padding-top: 36px;
			text-align: right;
		}
		button.botonenviar{
			font-weight: bold;
			color: white;
			background-color: #247386;
			margin-top: 60px;
                        padding:20px 0;
		}
		input#foto{
			position: relative;
			overflow: hidden;
		}
		button.botonseleccionar{
			color: white;
			background-color: #C2371E;
		}
                 .tipos{
                    margin:10px 40px 0;
                    text-align: center;
                }
	</style>
</head>
<body style="padding:0 60px">
	<div class="col-md-12" style="padding:80px 0">
		<div class="camino col-md-4">
                    <img src="<?=Url::to('@web/img/lg_camino.png')?>" alt="camino" width="390px" />
		</div>
                <div class="ayto col-md-4">
                    <p style="font-weight:bold; font-size:1.3em; text-align:center; padding: 44px 28px 0;">Lanzadera de Empleo y Emprendimiento Solidario del Ayuntamiento de Medio Cudeyo</p>
                </div>

                <div class="ayto col-md-2">
                    <img src="<?=Url::to('@web/img/lg_ayuntamiento.png')?>" alt="lanzaderas" width="156px" />
                </div>

                <div class="logo col-md-2 col-12">
                        <a href="https://www.cantabriaemprendedora.es/es/lanzaderas-cantabria-que-son-y-como-participar" target="_blank"><img src="<?=Url::to('@web/img/lg_lanzaderas3.png')?>" alt="lanzaderas" width="120px" /></a>
                </div>
	</div>

	<div class="col-md-12">
            <h1 style="text-align: center; font-weight: bold; padding-bottom: 60px;">¿Quieres que las Empresas te Encuentren?</h1>
	</div>

<div class="talentos-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	 <?= $form->field($model, 'id')->textInput(['maxlength' => true,'class'=>'form-control','id'=>'nombre','style'=>'display:none'])->label('') ?>
	<h3>Mi Perfil</h3>
	<div class="form-group">
		<div class="col-md-4 col-12">
<!--			<label for="nombre">Nombre</label>
			<input type="text" class="form-control" id="nombre"/>-->
                       
                         <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'class'=>'form-control','id'=>'nombre']) ?>
		</div>

		<div class="col-md-8 col-12">
<!--			<label for="apellidos">Apellidos</label>-->
			<!--<input type="text" class="form-control" id="apellidos"/>-->
                         <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true,'class'=>'form-control','id'=>'apellidos']) ?>
		</div>
	</div>

	<div class="form-group">
            <div class="col-md-4 col-12">
                    <div class=" tipos col-md-3">
                        <?= $form->field($model, 'particular')->checkBox(['label' => 'Particular','data-size'=>'small', 'class'=>'bs_switch','style'=>'margin-bottom:4px;', 'id'=>'active']) ?>
                    </div>
                    <div class="tipos col-md-3">
                        <?= $form->field($model, 'empresa')->checkBox(['label' => 'Empresa','data-size'=>'small', 'class'=>'bs_switch','style'=>'margin-bottom:4px;', 'id'=>'active']) ?>
                    </div>    
                        <!--$form->field($model, 'tipo')->dropDownList($tipo, ['prompt' => '','id' => 'tipo','class'=>'form-control' ]);--> 

		</div>

		<div class="col-md-8">
<!--			<label for="profesion">Profesión</label>
			<input type="text" class="form-control" id="profesion"/>-->
                        <?= $form->field($model, 'profesion')->textInput(['maxlength' => true,'class'=>'form-control','id' => 'profesion']) ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-4 col-12">
<!--			<label for="lanzadera">Lanzadera</label>
			<select class="form-control" id="lanzadera">
				<option>Medio Cudeyo</option>
			</select>-->
                        <?= $form->field($model, 'lanzadera')->dropDownList($lanzadera, ['prompt' => '','id' => 'lanzadera','class'=>'form-control' ]); ?>
		</div>

		<div class="col-md-4 col-12">
			<!--<label for="email">Email</label>-->
			<!--<input type="email" class="form-control" id="email"/>-->
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true,'class'=>'form-control','id' => 'email']) ?>
		</div>

		<div class="col-md-4 col-12">
<!--			<label for="tfno">Teléfono</label>
			<input type="text" class="form-control" id="tfno"/>-->  
                        <?= $form->field($model, 'telefono')->textInput(['maxlength' => true,'class'=>'form-control','id' => 'telefono']) ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-6 col-12">
<!--			<label for="linkedin">Linkedin</label>
			<input type="url" class="form-control" id="linkedin"/>-->
                        <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true,'class'=>'form-control','id' => 'linkedin']) ?>
		</div>

		<div class="col-md-6 col-12">
<!--			<label for="web">Web</label>
			<input type="url" class="form-control" id="web"/>-->
                        <?= $form->field($model, 'web')->textInput(['maxlength' => true,'class'=>'form-control','id' => 'web']) ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-12 col-12">
<!--			<label for="sobremi">Sobre Mi</label>
			<textarea class="form-control" id="sobremi" rows="3"></textarea>-->
                        <?= $form->field($model, 'sobremi')->textarea(['rows' => 3,'id'=>'sobremi']) ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-9">
<!--			<label for="carta">Carta de Presentación</label>
			<textarea class="form-control" id="carta" rows="8"></textarea>-->
                        <?= $form->field($model, 'carta')->textarea(['rows' => 8,'id'=>'carta']) ?>
		</div>
                <div class="col-md-3" style="display:flex; flex-direction: row; justify-content: center;margin-top: 50px">
                    <div style=";background-color: #f5f5f5;width: 150px;height: 170px;border: 1px solid #999999;display:block;"> 
                        <img src="<?= $ruta_foto_perfil?>" style="max-width:150px;width:150px;height:170px"/>
                    </div>
                    <button type="button" class="btn"  id="subirFoto" style="color: white; background-color: #EB8448; margin-left:2px;">Sube tu foto</button> 
                    <?= $form->field($model, 'imageFile')->fileInput(['id'=>'foto','style'=>'display:none'])->label('') ?>
                </div> 
	</div>

<!--	<div class="form-group">
           
            <button type="button" class="btn btn-primary"  id="subirFoto" style="color: white; background-color: #EB8448; margin-top: 60px; margin-bottom: 20px;margin-left:46%">Sube tu foto</button> 
             $form->field($model, 'imageFile')->fileInput(['id'=>'foto','style'=>'display:none'])->label('') ?>


           <div style="margin-left:44%;background-color: #f5f5f5;width: 150px;height: 150px;border: 1px solid #999999;display:block;"> 
               <img src=" $ruta_foto_perfil?>" id="img_perfil" style="max-width:150px;width:150px;height:150px"/>
            </div>
        </div>      -->
            
            
            
            
            
<!--            
		<button type="button" class="btn" id="subirFoto" onclick="ejecutaInputFile()" style="color: white; background-color: #EB8448; margin-top: 60px; margin-bottom: 20px;margin-left:46%">Sube tu foto</button>
    	<div style="margin-left:44%;background-color: #f5f5f5;width: 150px;height: 150px;border: 1px solid #999999;display:block;"> 
			<img id="uploadPreview1" src="" style="max-width:150px;width:150px;height:150px"/>
		</div>-->
	

		
        <div class="form-group">
           <div class="enviar col-md-12 col-12">
                <?= Html::submitButton('Guardar', ['class' => 'botonenviar btn btn-block']) ?>
           </div> 
            
            <div class="col-md-12" style="text-align:right; padding-top:8px">
                <span>Si quiere dar de baja su cuenta haga clic <a href="<?= Url::to(['talentos/delete_perfil','id'=>$model->id])?>">aquí</a></span>
            </div>
        </div>

	

	<div class="pie col-md-12">
            <div class="textopie col-md-5">
                <p class="enelpie">Agencia de Desarrollo local del Ayuntamiento de Medio Cudeyo</p>
                <p class="enelpie">Tlf: 942 522 833</p>
            </div>
            <div class="gobierno col-md-3">
                <img src="<?=Url::to('@web/img/lg_gobierno2.png')?>" alt="gobierno" width="160px"/>
            </div>
            <div class="emcan col-md-2">
                <a href="https://www.empleacantabria.es/" target="_blank"><img src="<?=Url::to('@web/img/lg_emcan2.png')?>" alt="emcan" width="200px"/></a>
            </div>
            <div class="cantabria col-md-2">
                <img src="<?=Url::to('@web/img/lg_cantabria2.png')?>" alt="cantabria" width="150px"/>
            </div>
        </div>
 

<?php ActiveForm::end(); ?>


</div>    
</body>
<script>
$( document ).ready(function() {
    $('#subirFoto').click(function(event) { 
        
       $('#foto').click();
            
    });
    
     
}); 
</script>
</html>
