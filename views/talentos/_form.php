<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Talentos */
/* @var $form yii\widgets\ActiveForm */
$tipo = ['Empresa'=>'Empresa','Particular'=>'Particular'];
$lanzadera = ['Medio Cudeyo'=>'Medio Cudeyo'];
$ruta_foto_perfil = Url::to('@web/img/'.$model->lanzadera.'/'.$model->apellidos.$model->nombre.$model->id.'.png');   
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

<div class="talentos-form" style="padding:30px 50px">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <!--$form->field($model, 'id')->textInput() -->
     <div class="form-group row">
        <div class="col col-sm-12">
             <div class="col col-sm-6">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
             </div>
             <div class="col col-sm-6">
                <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
             </div>
        </div> 
     </div>     
    <div class="form-group row">
          <div class="col col-sm-12">
               <div class="col col-sm-6">
                    <?= $form->field($model, 'profesion')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
               </div>
               <div class="col col-sm-2" style="display:flex; justify-content: space-around; margin-top:30px">
                   
                   <?= $form->field($model, 'particular')->checkBox(['label' => 'Particular','data-size'=>'small', 'class'=>'bs_switch','style'=>'margin-bottom:4px;', 'id'=>'active']) ?>
                    <?= $form->field($model, 'empresa')->checkBox(['label' => 'Empresa','data-size'=>'small', 'class'=>'bs_switch','style'=>'margin-bottom:4px;', 'id'=>'active']) ?>
               </div>   
              <div class="col col-sm-4">
                   <?= $form->field($model, 'lanzadera')->dropDownList($lanzadera, ['prompt' => '','id' => 'tipo_lanzadera','class'=>'form-control' ]); ?>
               </div>
          </div>
    </div>    
    <div class="form-group row">
          <div class="col col-sm-12">
               <div class="col col-sm-6">
                    <?= $form->field($model, 'telefono')->textInput(['class'=>'form-control']) ?>
               </div>
               <div class="col col-sm-6">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
               </div>    
          </div>
    </div>    
    <div class="form-group row">
          <div class="col col-sm-12">
               <div class="col col-sm-6">
                    <?= $form->field($model, 'web')->textInput(['maxlength' => true,'class'=>'form-control']) ?>

               </div>
               <div class="col col-sm-6">
                     <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
               </div>    
          </div>
    <!--</div>-->    
    <div class="form-group row">
        <div class="col col-sm-12">
            <div class="col col-sm-6">
                <?= $form->field($model, 'sobremi')->textarea(['rows' => 4]) ?>
            </div>
            <div class="col col-sm-6">
                <div class="col col-sm-3">
                    <button type="button" class="btn btn-primary"  id="subirFoto" style="margin-top: 25px; margin-left:46%">Sube tu foto</button> 
                    <?= $form->field($model, 'imageFile')->fileInput(['id'=>'foto','style'=>'display:none'])->label('') ?>
                </div>
                <div class="col col-sm-3">
                    <div style="border-radius: 5px;margin-left:44%;margin-top: 20px;background-color: #f5f5f5;width: 120px;height: 120px;border: 1px solid #999999;display:block;"> 
                        <img src="<?= $ruta_foto_perfil?>" style="border-radius: 5px;max-width:120px;width:120px;height:120px"/>
                    </div>
                </div>     
            </div>    
        </div>
    </div>    
    
    <div class="form-group row">
        <div class="col col-sm-12">
             <div class="col col-sm-12">
                <?= $form->field($model, 'carta')->textarea(['rows' => 10]) ?>
             </div>    
        </div>
   </div>    
   

    <div class="form-group" style="text-align: center">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
<script>
$( document ).ready(function() {
    $('#subirFoto').click(function(event) { 
        
       $('#foto').click();
            
    });
    
     
}); 
</script>