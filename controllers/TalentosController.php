<?php

namespace app\controllers;

use Yii;
use app\models\Talentos;
use app\models\TalentosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\User;
/**
 * AlumnosController implements the CRUD actions for Alumnos model.
 */
class TalentosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alumnos models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest || !User::isUserAdmin(Yii::$app->user->identity->getId())){
            return $this->redirect(['site/index']);
        }  
        
        $searchModel = new TalentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alumnos model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest || !User::isUserAdmin(Yii::$app->user->identity->getId())){
            return $this->redirect(['site/index']);
        }  
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alumnos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest || !User::isUserAdmin(Yii::$app->user->identity->getId())){
            return $this->redirect(['site/index']);
        }  
        
        $model = new Talentos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                // el archivo se subió exitosamente
                
            return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    
    }
        public function actionPerfil()
    {
//        if(Yii::$app->user->isGuest || !User::isUserUsuario(Yii::$app->user->identity->getId())){
//            return $this->redirect(['site/index']);
//        }  
       
         $model = new Talentos();    
           
        $email_logueado = Yii::$app->user->identity->getEmail();
       
        if ($model->load(Yii::$app->request->post())) {
           $accion = 'guardar';
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
             if(!empty($model->imageFile)){
               $model->upload();
            }      
             if($this->findModelEmail($email_logueado,$accion)){
                 
//                 if (($model = Modulosmatricula::findOne($id)) !== null) 
//                        return $model;
                 
              
                 $registro_actualizar = new Talentos;
                  
                 $registro_actualizar = Talentos::find()->where(['id' =>$model->id])->one();
                 
                 $registro_actualizar->nombre = $model->nombre;
                 $registro_actualizar->apellidos = $model->apellidos;
                 $registro_actualizar->profesion = $model->profesion;
                 
              //falta añadir en el model el check de empresa o particular
                 $registro_actualizar->lanzadera = $model->lanzadera;
                 $registro_actualizar->telefono = $model->telefono;
                 $registro_actualizar->email = $model->email;
                 $registro_actualizar->web = $model->web;
                 $registro_actualizar->linkedin = $model->linkedin;
                 $registro_actualizar->sobremi = $model->sobremi;
                 $registro_actualizar->carta = $model->carta;
                        
                $registro_actualizar->update();
                   
                $msg = "Registro Actualizado correctamente";
               
            }else{        
                
                  $model->save();
                    
                  $msg = "Registro Guardado correctamente";
            }
           
            return $this->render('perfil', ['model' => $model,'msg' => $msg]);
         
//           if($model->getTalentoduplicado($model->nombre, $model->apellidos)){
//              
//                
//                $msg = "Registro Actualizado correctamente";
//                return $this->render('perfil', ['model' => $model,'msg' => $msg]);
//                
//           }else{
//                $model->save();
//                $msg = "Registro guardado correctamente";
//                return $this->render('perfil', ['model' => $model,'msg' => $msg]);
//           }
//
        }else{
           $accion = "modelo";
           
            if($model = $this->findModelEmail($email_logueado,$accion)){
              
                return $this->render('perfil', [
                    'model' => $model,
                ]);
            }
   
        }   


    }

    public function actionDelete_perfil($id){
        
        $modelo_eliminar = new Talentos;          
        $modelo_eliminar = Talentos::find()->where(['id' =>$id])->one();
        if(!empty($modelo_eliminar)){
            $modelo_eliminar->delete();
            $msg= "Perfil de Talento eliminado con éxito";
            $model = new Talentos();
            return $this->render('perfil', ['model' => $model,'msg' => $msg]);
        }
        
        
    }
    /**
     * Updates an existing Alumnos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
       
        $model = $this->findModel($id);
     
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
             $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
             if(!empty($model->imageFile)){
                if ($model->upload()) {
                // el archivo se subió exitosamente
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }
            
            return $this->redirect(['update', 'id' => $model->id]);
      
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Alumnos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alumnos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Alumnos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Talentos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function findModelEmail($email,$accion)
    { 
        
       $form_vacio = new Talentos();
       $resultado = Talentos::find()->where(['email' => $email])->one();
    
       if(!empty($resultado)){
           
            if($accion == "modelo") {
                return $resultado;
                
            }else{
                return true;
            }    
            
       }else{
           
            if($accion == "guardar") {
                return false;
            }else{
                return $form_vacio;
            }    
       }
           
           

        
        
   

        throw new NotFoundHttpException('The requested page does not exist.');
    }


//        var_dump($_REQUEST);
//        exit;
//     if(isset($_FILES['archivo'])){
//        $ruta_origen = '../web/img/';
//        $carpeta = '../web/img/'.$lanzadera.'/';
//       
//        $fichero_origen ='../web/img/'.$lanzadera.'/'.$id.'/'.'jpg';
//        
//        if(!file_exists($carpeta))
//            $ruta_talento = mkdir($ruta_origen.$lanzadera,true);
// 
//         //$fichero_subido = $ruta_foto . basename($_FILES['archivo']['name']);
//        
//        if (file_exists($fichero_origen)) {
//            unlink($fichero_origen);
//        }
//        if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_origen)) {
//           return $this->redirect(Yii::$app->request->referrer);
//        } else {
//            echo "¡Posible ataque de subida de ficheros!\n";
//            return $this->redirect(Yii::$app->request->referrer);
//        }
//
//          
//     }else{
//         echo "no llega el fichero";
//     }
        
        
   
}